package com.testing;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
public class TestingAssignment {
	public static WebDriver driver;
	@BeforeClass
	public void launchBrowser() {
		
		driver = new ChromeDriver();
		driver.get("https://admin-demo.nopcommerce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
	}
	@Test(priority = 1)
	public void Login() {
		// driver.findElement(By.id("Email")).sendKeys("admin@yourstore.com");
		// driver.findElement(By.id("Password")).sendKeys("admin");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		String actual = driver.findElement(By.partialLinkText("John Smith")).getText();
		System.out.println(actual);
		String expt = "John Smith";
		AssertJUnit.assertEquals(actual, expt);
		driver.findElement(By.xpath("(//*[@class='nav-link'])[4]")).click();
	}
	@Test(priority = 2)
	public void categories() throws InterruptedException, IOException {
		driver.findElement(By.xpath(" //p[contains(text(),' Categories')]")).click();
		driver.findElement(By.partialLinkText("Add new")).click();
		File f = new File("/home/jehin/Documents/exceldata.xlsx");
		FileInputStream fin = new FileInputStream(f);
		XSSFWorkbook workbook = new XSSFWorkbook(fin);
		XSSFSheet sh = workbook.getSheetAt(0);
		int rows = sh.getPhysicalNumberOfRows();
		for (int i = 1; i < rows; i++) {
			String name = sh.getRow(i).getCell(0).getStringCellValue();
			String description = sh.getRow(i).getCell(1).getStringCellValue();
			String pricefrom = String.valueOf(sh.getRow(i).getCell(2).getNumericCellValue());
			String priceto = String.valueOf(sh.getRow(i).getCell(3).getNumericCellValue());
			String display = String.valueOf(sh.getRow(i).getCell(4).getNumericCellValue());
			driver.findElement(By.id("Name")).sendKeys(name);
			driver.findElement(By.id("Description_ifr")).sendKeys(description);
			Thread.sleep(5);
			WebElement parent = driver.findElement(By.id("ParentCategoryId"));
			Thread.sleep(5);
			Select s = new Select(parent);
			s.selectByIndex(4);
			WebElement price = driver.findElement(By.xpath("//label[@for='PriceFrom']"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView()", price);
			WebElement from = driver.findElement(By.xpath("(//input[@class='k-formatted-value k-input'])[2]"));
			boolean enabled = from.isEnabled();
			System.out.println(enabled);
			from.sendKeys(pricefrom);
			String val = from.getAttribute("value");
			System.out.println("This is value= " + val);
			WebElement to = driver.findElement(By.xpath("(//input[@class=\"k-formatted-value k-input\"])[3]"));
			boolean enabled2 = to.isEnabled();
			System.out.println(enabled2);
			to.sendKeys(priceto);
			WebElement displayorder = driver
					.findElement(By.xpath("(//input[@class=\"k-formatted-value k-input\"])[4]"));
			boolean enabled3 = displayorder.isEnabled();
			System.out.println(enabled3);
			displayorder.sendKeys(display);
			WebElement savebtn = driver.findElement(By.xpath("//button[@name='save']"));
			js.executeScript("arguments[0].scrollIntoView(true)", savebtn);
			savebtn.click();
			driver.findElement(By.id("SearchCategoryName")).sendKeys("Laptop");
			driver.findElement(By.id("search-categories")).click();
			
		}
	}
	@Test(priority = 3)
	public void Products() {
		driver.findElement(By.partialLinkText("Products")).click();
		driver.findElement(By.id("SearchProductName")).sendKeys("Build your own computer");
		WebElement dd = driver.findElement(By.id("SearchCategoryId"));
		Select s = new Select(dd);
		s.selectByIndex(2);
		driver.findElement(By.id("search-products")).click();
	}
	@Test(priority = 4)
	public void Manufactures() throws IOException {
		driver.findElement(By.xpath("//p[contains(text(),'Manufacturers')]")).click();
		driver.findElement(By.partialLinkText("Add new")).click();
		File f = new File("/home/jehin/Documents/exceldata.xlsx");
		FileInputStream fin1 = new FileInputStream(f);
		XSSFWorkbook workbook = new XSSFWorkbook(fin1);
		XSSFSheet sheet = workbook.getSheetAt(1);
		int rows = sheet.getPhysicalNumberOfRows();
		for (int i = 1; i < rows; i++) {
			String name = sheet.getRow(i).getCell(0).getStringCellValue();
			String description = sheet.getRow(i).getCell(1).getStringCellValue();
			driver.findElement(By.id("Name")).sendKeys(name);
			driver.findElement(By.id("Description_ifr")).sendKeys(description);
			driver.findElement(By.xpath("//button[@name='save']")).click();
			driver.findElement(By.id("SearchManufacturerName")).sendKeys("Dell");
			driver.findElement(By.id("search-manufacturers")).click();
			
		}
	}
	@Test(priority = 5)
	public void Logout() {
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
	}
	@AfterClass
	public void closebrowser() {
		driver.quit();
	}
}